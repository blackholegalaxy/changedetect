import { Record, Map as ImmutableMap, List as ImmutableList } from 'immutable';

export enum ThingStatus {
    TODO,
    DOING,
    DONE,
}

export interface ThingData {
    name: string;
    description: string;
    status: ThingStatus;
    details: ImmutableList<string>;
}

const DEFAULTS: ThingData = {
    name: '',
    description: '',
    status: ThingStatus.TODO,
    details: ImmutableList<string>(),
};

export class ThingRecord extends Record<ThingData>(DEFAULTS) implements Readonly<ThingData> {
    constructor(data: ThingData) {
        super(data);
    }
}

export type Things = ImmutableMap<string, ThingRecord>;
