import { Injectable } from '@angular/core';
import { MyStore } from '../store.factory';
import { INCREMENT_COUNT } from '../store.actions';

@Injectable()
export class OtherAction {
    constructor(
        private store: MyStore
    ) { }

    public increment(): void {
        this.store.dispatch({ type: INCREMENT_COUNT });
    }
}
