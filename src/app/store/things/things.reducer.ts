import { buildReducer } from '../store.common';
import {
    SET_THING_PROPERTY,
    PropertySetAction,
    LOADING_THINGS,
    ADD_DETAILS_TO_THING,
    THINGS_LOADED,
    ItemLoadAction,
    ListAppendAction
} from '../store.actions';
import { ThingData, ThingRecord, Things } from './things.model';
import { Reducer, AnyAction } from 'redux';
import * as Immutable from 'immutable';

const REDUCERS: { [key: string]: Reducer<Things> } = {
    [SET_THING_PROPERTY]: (state, action: PropertySetAction<ThingData>) => state
        .update(action.id, record => record.merge(action.values)),

    [LOADING_THINGS]: (state, action) => state.clear(),

    [THINGS_LOADED]: (state, action: ItemLoadAction<ThingData>) => Object.keys(action.items)
        .reduce((transitiveState, id) => transitiveState.set(id, new ThingRecord(action.items[id])), state),

    [ADD_DETAILS_TO_THING]: (state, action: ListAppendAction<ThingData, 'details'>) => state
        .update(action.id, record => record.set('details', record.details.concat(action.values.details))),
};

export const thingsReducer = buildReducer(REDUCERS, Immutable.Map<string, ThingRecord>());
