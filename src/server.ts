import * as express from 'express';
import * as http from 'http';
import * as bunyan from 'bunyan';

const app = express();
const server = http.createServer(app);
const logger = bunyan.createLogger({ name: 'app' });

const thingsRouter = express.Router();

thingsRouter.get('/', (req, res) => {
    res.json([{
        id: '1',
        status: 'DONE',
        name: 'First',
        description: 'Something',
    }, {
        id: '2',
        status: 'DOING',
        name: 'Second',
        description: 'Something',
    }, {
        id: '3',
        status: 'TODO',
        name: 'Third',
        description: 'Something',
    }, {
        id: '4',
        status: 'TODO',
        name: 'Fourth',
        description: 'Something',
    }, {
        id: '5',
        status: 'DONE',
        name: 'Fifth',
        description: 'Something',
    }, {
        id: '6',
        status: 'DONE',
        name: 'Sixth',
        description: 'Something',
    }]);
});

app.use((req, res, next) => {
    logger.debug('Serving', { method: req.method, url: req.url });
    next();
});

app.use('/things', thingsRouter);

server.listen(3000, () => logger.info('Listening', server.address()));
