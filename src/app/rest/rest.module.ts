import { NgModule } from '@angular/core';
import { ThingsService } from './things.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
    imports: [
        HttpClientModule,
    ],
    providers: [
        ThingsService,
    ]
})
export class RestModule {

}
