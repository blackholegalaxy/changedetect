import { Action } from 'redux';

export const SET_THING_PROPERTY = 'SET_THING_PROPERTY';
export const SET_OTHER_PROPERTY = 'SET_OTHER_PROPERTY';
export const LOADING_THINGS = 'LOADING_THINGS';
export const THINGS_LOADED = 'THINGS_LOADED';
export const INCREMENT_COUNT = 'INCREMENT_COUNT';
export const ADD_DETAILS_TO_THING = 'ADD_DETAILS_TO_THING';

export interface PropertySetAction<T> extends Action {
    readonly type: typeof SET_THING_PROPERTY;
    readonly id: string;
    readonly values: Partial<T>;
}

export interface ListAppendAction<T, K extends keyof T> extends Action {
    readonly type: typeof ADD_DETAILS_TO_THING;
    readonly id: string;
    readonly values: Pick<T, K>;
}

export interface ItemLoadAction<T> extends Action {
    readonly type: typeof THINGS_LOADED;
    readonly items: { [key: string]: T };
}
