import { buildReducer, ReducersMap } from '../store.common';
import { OtherRecord } from './other.model';
import { INCREMENT_COUNT } from '../store.actions';

const REDUCERS: ReducersMap<OtherRecord> = {
    [INCREMENT_COUNT]: (state, action) => state.increment(),
};

export const otherReducer = buildReducer(REDUCERS, new OtherRecord({ count: 0 }));
